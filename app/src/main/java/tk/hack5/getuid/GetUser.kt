package tk.hack5.getuid

object GetUser {
    @JvmStatic
    private external fun getUser(): String
    val username: String
    init {
        System.loadLibrary("getuser")
        username = getUser()
    }
}
