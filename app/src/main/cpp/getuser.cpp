#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <jni.h>

extern "C" JNIEXPORT jstring JNICALL Java_tk_hack5_getuid_GetUser_getUser(JNIEnv* env) {
    uid_t uid = geteuid();
    struct passwd *user;
    if (uid == -1)
        return NULL;
    user = getpwuid(uid);
    return env->NewStringUTF(user->pw_name);
}